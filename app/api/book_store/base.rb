# frozen_string_literal: true

module BookStore
  class Base < Grape::API
    mount BookStore::V1::Books
  end
end